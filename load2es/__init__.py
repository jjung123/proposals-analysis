__version__ = '0.1.0'

import csv
from elasticsearch import helpers, Elasticsearch
from glob import glob


def csv_reader(file_name, index_name):
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    with open(file_name, 'r') as outfile:
        reader = csv.DictReader(outfile)
        helpers.bulk(es, reader, index=index_name, doc_type="doc")


if __name__ == "__main__":
    for f in glob("*.csv"):
        # print(f)
        csv_reader(f, "proposals")
